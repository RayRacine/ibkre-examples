#lang typed/racket/no-check

(provide
 us-stock-contract-template
 us-option-contract-template)

(require
  (only-in ibkre/data/contract
           Contract))

(: us-stock-contract-template (-> String Contract))
(define (us-stock-contract-template symbol)
  (Contract 0 symbol 'STK "" 0.0 "" "" "SMART" "" "USD"
            "" "" #f 'ISIN "" "" (list) #f))

(: us-option-contract-template (-> Symbol String String Float String Contract))
(define (us-option-contract-template symbol exch expire strike right)
  (Contract 0 (symbol->string symbol) 'OPT expire strike right "100" exch "" "USD" "" "" #f
            'ISIN "" "" (list) #f))
