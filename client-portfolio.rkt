#lang syndicate

(require
  "config.rkt"
  "client-logging.rkt"
  ibkre/logging
  (only-in  ibkre/session
            TwsSess)
  (only-in ibkre/communicate
           spawn-driver)
  (only-in ibkre/txid/gen-tx-rand
           gen-rid)
  (only-in ibkre/comm/msgs
           TwsSend)
  (only-in ibkre/api/req/account
           positions-msg positions-cancel-msg
           account-summary-msg
           account-summary-cancel-msg)
  (only-in ibkre/api/msg
           Error
           Position PositionEnd
           AccountSummary AccountSummaryEnd))

(message-struct Nav (rid portfolios))
(message-struct Pos (rid positions))

(define (spawn-portfolio-positions client gid)
  (spawn #:name '(portfolio positions)

         (define my-rid (gen-rid))

         (field
          (positions '()))

         (on-start
          (send! (TwsSend client positions-msg)))         
         
         (stop-when
          (message (PositionEnd)))

         (on-stop
          (send! (TwsSend client positions-cancel-msg))
          (send! (Pos gid (positions))))

          (on (message ($ p (Position _ _ _ _)))
              (positions (cons p (positions))))

          ))

(define (spawn-portfolio-nav client gid)
  
  (spawn #:name '(portfolio nav)

         (define my-rid (gen-rid))
         
         (field
          (portfolio '()))

         (on-start
          (send! (TwsSend client (account-summary-msg my-rid "All" "NetLiquidation"))))
         
         (stop-when
          (message (AccountSummaryEnd my-rid)))
         
         (on-stop
          (send! (TwsSend client (account-summary-cancel-msg my-rid)))
          (send! (Nav gid (portfolio))))
                   
         (on (message ($ as (AccountSummary my-rid _ _ _ _)))
             (log-ibk/core-error "Accnt Summ: ~a" as)
             (portfolio (cons as (portfolio))))

          (on (message (Error my-rid $cd $msg))
              (log-ibk/core-info "Error: ~a ~a" cd msg))             
              
         ))

(define (portfolio-show nav pos)
  (log-ibk/core-error "============== NAV =============")
  (for ([n nav])
    (log-ibk/core-error "- ~a" n)) 
  (log-ibk/core-error "--------------------------------") 
  (for ([p pos])
    (log-ibk/core-error "- ~a" p))
  (log-ibk/core-error "================================"))

  
(define (spawn-portfolio client host port)
  
  (spawn #:name 'portfolio

         (field
          (portfolio-nav #f)
          (portfolio-pos #f))
     
         (assert (TwsSess client host port 'Demand))

         (define my-rid (gen-rid))                 

         (stop-when-true
          (and (portfolio-nav) (portfolio-pos)))

         (on-stop
          (portfolio-show (portfolio-nav) (portfolio-pos)))
         
         (on (asserted (TwsSess client host port 'Supply))
             (log-ibk/core-error "Portfolio")
             (spawn-portfolio-nav client my-rid)
             (spawn-portfolio-positions client my-rid))
         
         (on (message (Nav my-rid $nav))
             (portfolio-nav nav))
         
         (on (message (Pos my-rid $pos))
             (portfolio-pos pos))
         
         ))

;;(start-optch-logging 'debug)
(log-ibk/core-error "Starting ...")
(spawn-driver)
(spawn-portfolio def-client def-host def-port)
