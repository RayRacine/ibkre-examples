#lang scribble/manual

@(require racket/sandbox
          scribble/example)

@(define my-evaluator
   (parameterize ([sandbox-output 'string]
                  [sandbox-error-output 'string]
                  [sandbox-memory-limit 200])
     (make-evaluator 'racket/base)))

@author[(author+email "Raymond Racine" "ray.racine@gmail.com")]
 	 	
@title{Using Ibkre}
This Intro / First Tutorial shows how to use the Ibkre Library to interact simply and efficiently with the IBK API.

The IBK API is an asynchronous API.  As a consequence a client of the IBK API fires requests from one thread of execution, and some unbounded time later responses are received from the IBK server on another thread of execution in your program.
Efficient utililization of the API suggests the client code is to be written concurrently where numerous API requests and responses are all happening at-the-same-time.
Howeer, concurrent, asynchronous code can be hard to wrangle when everything-is-happening-at-once.  

There are various coding paradigms one can use to prevent melting your brain when coding at the lowest levels with an asynchronous API.
Ibkre takes an event based, reactive, Actor focused approach to writing async, concurrent programs which makes using the IPK API simple.

Ibkre Library itself uses the wonderful Syndicate Actor Library by Tony Garnock-Jones.

After a brief background introduction on Actors, we will get to our first quick example of code.

@section{Conceptual Picture}

So let's break down what an event based, reactive, actor based system is all about.

@subsection{Actors}

Actors are a way of structuring code in some programming language similar how one may structure their code in some languages via Classes and Objects.
There is nothing special about Actors, in fact one may write an Actor system in pretty much any language.  

The essential characteristics of the Actor model are:
@itemlist[
 @item{Actor stores internal state much like a Class/Object.}
 @item{An Actor receives and acts on messages received from other Actors and process those messages one message at a time.}
 @item{An Actor may send messages to other Actors.}
 @item{An Actor may create other Actors.}
 @item{A program of Actors are all sending, receiving and processing messages all at the same time.}
 ]

@subsection{Event Based, Reactive}

A typical program in say Java or Python is all about calling methods and/or procedures in some sequence.  It sort of pushes itself along.  Some top level "main" calls something which calls something, which calls something ...
The program ends when the top level "main" returns.

A reactive program inverts the logic, it sort of pulls itself along, by reacting to a sequence of events.  A reactive program ends when there are no more events.  Events are the messages in our Actor Model.

Our Actors are independent threads of execution that send and react to message events.
For example, we might have several Actors responding to incoming IBK Tick data concerning various underlying instruments, while another Actor is placing an order, while yet another Actor is monitoring margin requirements, all running at the same time.

@section{Portfolio NAV And Positions}

Our first Ibkre program will do a simple portfolio status query, pecifically, query all positions in all accounts, and query the net liquidation value of all accounts.
To do this via the IBK API requires two api calls, "Account Summary" and "Positions". These API calls are independent of each other and may be called in any order or in parallel.  Parallel is more efficient.

@subsection{Program Overview}

When run our program will create a top level Actor called Portfolio.  The Portfolio Actor on starting creates two more Actors called Portfolio Positions and Portfolio NAV respectively.

The Portfolio Positions Actor on starting will send a Positions API request to the IBK server, listen for and collect up all Position event messages received from the server, and will stop upon receiving a PositionEnd message from IBK.
Finally, as it stops it will send a message back to the Portfolio Actor (the Actor which created it) containing all the collected positions as well as sending an IBK API PositionCancel message to the IBK server.

In similar fashion the Portfolio NAV Actor upon starting will send an AccountSummary request to IBK via the API, listen for and collect net liquidation responses from IBK, one for each Account, and will stop upon receipt of an AccountSummaryEnd API message from IBK.  As it stops it will send a message to the Portfolio Actor (again the Actor which created it) containing all the collection account summary information as well as sending an AccountSummaryCancel message to the IBK server.

The original Portfolio Actor upon receiving the information from the two child Actors prints the portfolio net liquidation value and positions and then stops, terminating the program.

@subsection{Defining An Actor}

Ibkre uses the Syndicate Actor System to create an Actor.
Defining an Actor follows a basic pattern: the internal state it maintains, what it does on starting, what messages it reacts to and how to process them, what makes it stop, what it does as it is stopping.

@subsection{Positions Actor}

@codeblock[#:line-numbers 1]|{
  (spawn #:name '(portfolio positions)

         (define my-rid (gen-rid))

         (field
          (positions '()))

         (on-start
          (send! (TwsSend client positions-msg)))         
         
         (stop-when
          (message (PositionEnd)))

         (on-stop
          (send! (TwsSend client positions-cancel-msg))
          (send! (Pos gid (positions))))

          (on (message ($ p (Position _ _ _ _)))
              (positions (cons p (positions))))

          )
}|

Line 1: Defines a function class spawn-contract-details which takes a client id, a unique transaction id and a IBK API Contract as arguments.

Line 3: Generates a unique transaction identifier to use in the IBK API call.

Lines 5-6: Creates a field (variable) named positions to hold a list of the positions.

Lines 8-9: When the Actor starts it sends a IBK API Positions request.

Lines 11-12: The Actor stops and exits when it receives a PostionEnd message from the IBK API.

Lines 14-16: When the Actor stops (from above condition) it sends a API PositionsCancel to the IBK server. In addition, the list of all of the collected postions are sent out in a Pos message to any other Actor who is listening for it.

Lines 18-19: Everytime a Positions message is received from the IBK Server (from our intial on start request) we append the postion in a list.

Notice there are no explicit sleeps in the above. The Actor responds to an incoming position message as soon as it is received from the IBK server and is otherwise idle.


@subsection{Portfolio NAV}

Next we define an Actor which fetches all our account’s Net Liquidation via the API. We will only point out the highlights for this Actor.

@codeblock[#:line-numbers 1]|{
(define (spawn-portfolio-nav client gid)
  
  (spawn #:name '(portfolio nav)

         (define my-rid (gen-rid))
         
         (field
          (portfolio '()))

         (on-start
          (send! (TwsSend client (account-summary-msg my-rid "All" "NetLiquidation"))))
         
         (stop-when
          (message (AccountSummaryEnd my-rid)))
         
         (on-stop
          (send! (TwsSend client (account-summary-cancel-msg my-rid)))
          (send! (Nav gid (portfolio))))
                   
         (on (message ($ as (AccountSummary my-rid _ _ _ _)))             
             (portfolio (cons as (portfolio))))

          (on (message (Error my-rid $cd $msg))
              (log-ibk/core-info "Error: ~a ~a" cd msg))             
              
         ))
}|

Line 8: Again we define a field called ’portfolio’ and initialize it to an empty list. IBK’s API will send one response per account, so we collect here in this field in a list.

Line 11: When our Actor starts it sends an AccountSummaryRequest to the IBK API.

Line 14: The Actor will stop upon receiving an AccountSummaryEnd message from IBK.

Line 17-18: Now that the Actor knows when to stop, we tell it what to do as it stops. First is sends an IBK API AccountSummaryCancel message and then it sends the list of collect IBK reponses to any other interested Actors by sending a ’Nav’ message.

Line 20: Our Actor listens for any incoming AccountSummary reponses from the IBK API Server. The ($ as (AccountSummary my-rid ...) part is a "pattern match" which will be explained in further detail later.
What is important here is we not just matching ANY AccountSummary response but only those in response to out specific request.
In other words, other Actors may have sent their own AccountSummary requests but we are only interseted in our responses. (FWIW, it is easy to define an Actor that will listen for ALL Tick or AccountSummary responses regardless of which Actor send the request.)

Line 21: We prepend the AccountSummary message to our list.

Line 23: Here we listen for any Error messages from the IBK server that have our original request id in them. In other words, we are listening for any errors arising from our request and not any errors form other Actor’s requests.

@subsection{One Actor To Rule Them All}

Now that we have our two Actors to fetch the Account Liquidation Values and the Account Positions, we now define top level Actor to orchestrate the whole thing, in a sense our "main" Actor.

@codeblock[#:line-numbers 1]|{
(define (spawn-portfolio)
  
  (spawn #:name 'portfolio

         (field
          (portfolio-nav #f)
          (portfolio-pos #f))
     
         (assert (TwsSess client host port 'Demand))

         (define my-rid (gen-rid))                 

         (stop-when-true
          (and (portfolio-nav) (portfolio-pos)))

         (on-stop
          (portfolio-show (portfolio-nav) (portfolio-pos)))
         
         (on (asserted (TwsSess client host port 'Supply))             
             (spawn-portfolio-nav client my-rid)
             (spawn-portfolio-positions client my-rid))
         
         (on (message (Nav my-rid $nav))
             (portfolio-nav nav))
         
         (on (message (Pos my-rid $pos))
             (portfolio-pos pos))
         
         ))
}|     

                              
Lines 6-7:  Are two fields we use to hold the data from our two child Actors.

Lines 9: This will be explained later in more detail.  But for now understand that by asserting this TwsSess data record we
are asking the framework to connect to the IBK API server.

Line 14: In our previous Actor definitions declared them to stop when they received the respective API End messages from the IBK server.
However, our top level Actor never talks to IBK API.  Our top level Actor is "done" when it receives the account
NAVs and positions from its child Actors, who do the actual API interaction.  Here we 

Line 17: When we stop show (print) the data to the console.

Line 19: Again will be explained later in further datail.  For now, accept that Line 9 above asked the framework to create a conection to the IBK server
and here Line 19 says what to do when we are notifed that the connection is ready.  It doesn't matter how long it takes to establish the connection.  When
the connection is established by the framework we are immediately notified.

Line 20-21: Here we launch our two Actors that we defined above after the framework has notified us that the connection to IBK is ready.
When an Actor is spawned it starts to run immediately reacting to messages in "background".
Line 21 spawns the NAV Actor, but it does not wait until that Actor is done, it immediately then spawns the Positions Actor.
All of our Actor, until they stop, are all running at the "same time".

Line 24: Listens for NAV info and saves it in a field.

Line 28: Listens for the Positions info and also saves in in a field. (Remember when both fields have been received Line 14 will be satified stopping
our Actor.   Line 17 states we will print it to console when we stop.

@section{The Complete Program}

Here is the complete, runnable program.

@codeblock[#:line-numbers 1]|{
#lang syndicate

(require
  "client-logging.rkt"
  ibkre/logging
  (only-in  ibkre/session
            TwsSess)
  (only-in ibkre/communicate
           spawn-driver)
  (only-in ibkre/txid/gen-tx-rand
           gen-rid)
  (only-in ibkre/comm/msgs
           TwsSend)
  (only-in ibkre/api/req/account
           positions-msg positions-cancel-msg
           account-summary-msg
           account-summary-cancel-msg)
  (only-in ibkre/api/msg
           Error
           Position PositionEnd
           AccountSummary AccountSummaryEnd))

(define-values (client host port)
  (values 1 "localhost" 7497))

(message-struct Nav (rid portfolios))
(message-struct Pos (rid positions))

(define (spawn-portfolio-positions client gid)
  (spawn #:name '(portfolio positions)

         (define my-rid (gen-rid))

         (field
          (positions '()))

         (on-start
          (send! (TwsSend client positions-msg)))
         
         (stop-when
          (message (PositionEnd)))

         (on-stop
          (send! (TwsSend client positions-cancel-msg))
          (send! (Pos gid (positions))))

          (on (message ($ p (Position _ _ _ _)))
              (positions (cons p (positions))))

          ))

(define (spawn-portfolio-nav client gid)
  
  (spawn #:name '(portfolio nav)

         (define my-rid (gen-rid))
         
         (field
          (portfolio '()))

         (on-start
          (send! (TwsSend client (account-summary-msg my-rid "All" "NetLiquidation"))))
         
         (stop-when
          (message (AccountSummaryEnd my-rid)))
         
         (on-stop
          (send! (TwsSend client (account-summary-cancel-msg my-rid)))
          (send! (Nav gid (portfolio))))
                   
         (on (message ($ as (AccountSummary my-rid _ _ _ _)))
             (portfolio (cons as (portfolio))))

          (on (message (Error my-rid $cd $msg))
              (log-ibk/core-error "Error: ~a ~a" cd msg))             
              
         ))

(define (portfolio-show nav pos)
  (log-ibk/core-info "============== NAV =============")
  (for ([n nav])
    (log-ibk/core-info "- ~a" n)) 
  (log-ibk/core-info "--------------------------------") 
  (for ([p pos])
    (log-ibk/core-info "- ~a" p))
  (log-ibk/core-info "================================"))

  
(define (spawn-portfolio)
  
  (spawn #:name 'portfolio

         (field
          (portfolio-nav #f)
          (portfolio-pos #f))
     
         (assert (TwsSess client host port 'Demand))

         (define my-rid (gen-rid))                 

         (stop-when-true
          (and (portfolio-nav) (portfolio-pos)))

         (on-stop
          (portfolio-show (portfolio-nav) (portfolio-pos)))
         
         (on (asserted (TwsSess client host port 'Supply))
             (spawn-portfolio-nav client my-rid)
             (spawn-portfolio-positions client my-rid))
         
         (on (message (Nav my-rid $nav))
             (portfolio-nav nav))
         
         (on (message (Pos my-rid $pos))
             (portfolio-pos pos))
         
         ))

(spawn-driver)
(spawn-portfolio)
}|