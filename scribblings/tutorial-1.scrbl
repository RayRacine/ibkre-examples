#lang scribble/manual

@(require racket/sandbox
          scribble/example)

@(define my-evaluator
	    (parameterize ([sandbox-output 'string]
                     [sandbox-error-output 'string]
            	           [sandbox-memory-limit 200])
     (make-evaluator 	   'racket/base)))
     
